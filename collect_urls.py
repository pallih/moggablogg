"""Tries to find all subdomains of blog.is that have blogs
Exports to a txt file with one domain per line"""

import requests
import lxml.html
from urllib.parse import urlparse
import dataset
import grequests
import logging
import sys


logging.basicConfig(stream=sys.stdout, level=logging.INFO)
logger = logging.getLogger('collect_urls')

TOP_LIST_URLS = ['http://blog.is/forsida/new.html?num=400', 'http://blog.is/forsida/top.html?num=400']

s = requests.session()

db = dataset.connect('sqlite:///moggablog.db')
table = db['blogs']


def exception_handler(request, exception):
    logger.warning("Request failed")
    url = urlparse(request.url)
    table.update(dict(domain=url[1], spider_friends=1), 'domain')


def scrape_top_lists(urls):
    """Scrapes the popular top list and the new blog list"""
    batch = []
    for url in urls:
        response = s.get(url)
        root = lxml.html.fromstring(response.text)
        found_urls = root.xpath("//table[@class='blog-popular']/tr/td/a")
        for found_url in found_urls:
            data = {}
            parsed = urlparse(found_url.attrib['href'])
            data['domain'] = parsed.netloc
            data['spider_friends'] = None
            batch.append(data)
    return batch


def find_friends(responses):
    for response in responses:
        logger.debug('Doing url: {}'.format(response.url))
        # Some discontinued blogs redirect to the frontpage.
        # We just mark those as done - we should really parse this from
        # the headers, but whatever.
        if 'forsida' in response.url:
            # original url
            url = urlparse(response.history[0].url)
            table.update(dict(domain=url.netloc, spider_friends=1), 'domain')
            continue
        # Some use a custom domain - skip the friend find
        if 'blog.is' not in response.url:
            # original url
            url = urlparse(response.history[0].url)
            table.update(dict(domain=url.netloc, spider_friends=1), 'domain')
            continue

        root = lxml.html.fromstring(response.text)
        friends = root.xpath("//div[@id='Blog-friends-box']//ul[@class='box-list personal']/li")
        logger.debug("{} - friends: {}".format(response.url, len(friends)))
        if len(friends) > 0:
            batch = []
            for friend in friends:
                data = {}
                parsed = urlparse(friend[0].attrib['href'])
                data['domain'] = parsed[1]
                batch.append(data)
            for item in batch:
                table.insert_ignore(item, 'domain')
        url = urlparse(response.url)
        table.update(dict(domain=url[1], spider_friends=1), 'domain')


def prepare_responses(todo):
    urls = []
    for url in todo:
        urls.append('http://{}'.format(url['domain']))
    if urls:
        logger.info("Finding friends for {} url(s)".format(len(urls)))
        rs = (grequests.get(u, timeout=5) for u in urls)
        responses = grequests.imap(rs, exception_handler=exception_handler)
        return responses


def export_db_to_csv(filename):
    results = db.query('SELECT domain from blogs')
    logger.info('Exporting to {}'.format(filename))
    with open(filename, "w") as f:
        for result in results:
            # Why the lstrip, you ask? Well... there is a subdomain that
            # somewhere in the pipe is wrongly parsed, leaving it with
            # a prefixed dash. Why not fix it there, you ask?
            # Well... I'm feeling lazy.
            f.write("{}\n".format(result['domain'].strip().lstrip("-")))


if __name__ == '__main__':
    # Find domains on the top list and new list
    top_list_urls = scrape_top_lists(TOP_LIST_URLS)
    for item in top_list_urls:
        table.insert_ignore(item, 'domain')

    while True:
        todo_domains = db.query('SELECT * FROM blogs WHERE spider_friends != 1')
        responses = prepare_responses(todo_domains)
        if responses:
            find_friends(responses)
        else:
            logger.info("All done with collecting urls!")
            export_db_to_csv('data/blog_is_domains.txt')
            break
