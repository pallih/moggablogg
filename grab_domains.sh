#!/bin/sh

export USER_AGENT="Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/533.20.25 (KHTML, like Gecko) Version/5.0.4 Safari/533.20.27"
export DATE=`date +%Y-%m-%d`
export CRAWL_DIRECTORY="data/crawls/$DATE"
export SAVE_DB="$CRAWL_DIRECTORY/$DATE.db"
export SAVE_LOG="$CRAWL_DIRECTORY/$DATE.log"
export URL_FILE="data/blog_is_domains.txt"

# --append-output "$SAVE_LOG" \
#         --wait 0.2 \

# --exclude-domains scorecardresearch.com,facebook.com,google-analytics.com,mbl.is,www.mbl.is \

while IFS=, read domain
do
    export DOMAIN="${domain%""${domain##*[![:space:]]}""}"
    export DIRECTORY="$CRAWL_DIRECTORY/$DOMAIN"
    export WARC_NAME="$DIRECTORY/$DOMAIN-$DATE"
    mkdir -p "$DIRECTORY"
    wpull "$domain" \
        --warc-file "$WARC_NAME" \
        --warc-append \
        --warc-header 'moggabloggið https://github.com/pallih/moggablogg' \
        --no-check-certificate \
        --no-robots \
        --user-agent "$USER_AGENT" \
        --waitretry 600 \
        --page-requisites \
        --page-requisites-level 1 \
        --recursive \
        --span-hosts-allow page-requisites \
        --escaped-fragment \
        --strip-session-id \
        --no-parent \
        --reject-regex "\/forsida\/samband\.html|\/password_remind\.html|js\/counter\.js|js\/newsblog-flagging\.js|js\/bfriends\.js|lib\/comments\/watch" \
        --exclude-domains mbl.is,www.mbl.is \
        --domains is \
        --hostnames "$domain",t.blog.is \
        --tries 3 \
        --retry-connrefused \
        --retry-dns-error \
        --timeout 60 \
        --session-timeout 21600 \
        --delete-after \
        --database "$SAVE_DB" \
        --append-output "$SAVE_LOG" \
        --http-compression \
        --verbose \
        --report-speed bits \
        --concurrent 20 \
        --monitor-disk 1000 \
        --monitor-memory 10000 &
        wait $!
    echo "Archived $domain"
done < "$URL_FILE"


